# On-boardining assessment #

## User Story

John, Bob, and Walter are beanie baby investors. They want to corner the market by buying all 
the beanie babies, hoping to be able to ebay them for more in the future (and make a profit). 
John is the richest of the three. Bob is middle class. Walter is poor. Because of this, the
men decide to split the cost of buying these beanie babies weighted by their net worth. John
will contribute the most, Bob will contribute some, and Walter will contribute a little. 

Their current net worths are the following:

| Name   | Net Worth  |
|--------|------------|
| John   | 10,000,000 |
| Bob    | 300,000    |
| Walter | 10,000     |

To make this easier, the men decide they need a calculator that accepts a list of beanie babies,
and outputs the amount each man owes per beanie baby, as well as the total each man owes. 


## Acceptance Criteria:	

* Calculator must be a PHP CLI program that accepts, at the least, a text file in the appropriate format (see `data/beaniebabies.txt`) containing the beanie baby values. It _can_ accept more inputs if you think it necessary
* Output should be the amount each man pays for each beanie baby, along with the total they will each be paying. format of the output is up to you.
* All men have to pay _at least_ 1 penny for each beanie baby
* The amount they pay per beanie baby is a ratio of individual net worth/total net worth of all three men (with precision up to 2 decimal places; aka 80.99%, 1.42%, etc).
* The men _might_ want to use this logic for future investments; overengineering is encouraged for this task.
* Use solid OOP principles; this should not be completed as if it were a one-off script. 
* You may not use any frameworks (this should be started from a blank page), but you **may use composer libraries** where you deem appropriate (for example; phpunit). 

This task should take no more than a few hours. Bear in mind that you are not only being judged on accomplishing the goal; but also on your attention to detail, and following generally accepted best practices. 


## Assumptions:
* No beanie baby costs less than 3 cents
* No beanie baby is free (costs nothing)
* All three men have enough money to cover their share of the total cost of all beanie babies

## Submission:

Please create a branch from master named in the format of `<first name>_<last name>` (for example: `dan_marra`), and submit a PR to dmarra@kapitus.com when you have completed the task. 